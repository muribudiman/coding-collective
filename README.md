## Install
	- git clone https://gitlab.com/muribudiman/coding-collective.git
	- cd coding-collective
    - composer install
	- cp .env.example .env
    - php artisan key:generate
    - Kuncinya akan ditulis secara otomatis di file .env Anda.
## Database
    - buat database laravel9_cc
    - php artisan migrate --seed
    - php artisan storage:link
## Running Seeders
    - php artisan migrate:refresh --seed
## Database Config
    - config/database.php
```
    edit .env
    
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=laravel9_cc
    DB_USERNAME=root
    DB_PASSWORD=
```


```
## Admin panel Laravel
```
    username : admin@admin.com
    password : admin

```

## Tools
```
    Php framework 	: Laravel 9.x, Jetstream, Inertia (Vue)
    Php 			: >= 8.0 
    editor 			: Visual Studio Code
    os 				: Ubuntu
    webserver		: Nginx
	
```

# Node
####node Version
#node -> stable (-> v18.14.0)

```php
#npm install && npm run build  

```