<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'amount',
        'type',
    ];

    public $timestamps = true;

    public $incrementing = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'formattedAmountAt',
        'formattedCreatedAt',
        'formattedUpdatedAt',
    ];

    public function getFormattedCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M Y H:i:s');
    }
	
	public function getFormattedUpdatedAtAttribute($value)
    {
        return Carbon::parse($this->updated_at)->format('d M Y H:i:s');
    }
	
	public function getFormattedAmountAtAttribute($value)
    {
        return generalMoney($this->amount, 2);
    }

    public function scopeWhereSearch($query, $search)
    {
        foreach (explode(' ', $search) as $term) {
            $query->where('orders.order_id', 'LIKE', '%'.$term.'%');
        }
    }
    
    public function scopeApplyFilters($query, array $filters)
    {
        $filters = collect($filters);
        if ($filters->get('search')) {
            $query->whereSearch($filters->get('search'));
        }
    }

    public function scopePaginateData($query, $limit)
    {
        if ($limit == 'all') {
            return collect(['data' => $query->get()]);
        }

        return $query->paginate($limit);
    }

    public static function createWebapp($request) {
        $data = $request;

        if($model = self::create($data)) {
            $model->fresh();
            OrderUser::create([
                'user_id' => $data['user_id'],
                'order_id' => $model->id,
            ]);

            if($balance = Balance::where('user_id', $data['user_id'])->first()) {
                $balance->balance += $model->amount;
                $balance->save();
            } else {
                Balance::create([
                    'user_id' => $data['user_id'],
                    'balance' => $model->amount,
                ]);
            }

			return $model;
		}
		
        return false;
    }


    public static function createWithdrawal($request) {
        $data = $request;

        if($model = self::create($data)) {
            $model->fresh();
            OrderUser::create([
                'user_id' => $data['user_id'],
                'order_id' => $model->id,
            ]);

            if($balance = Balance::where('user_id', $data['user_id'])->first()) {
                $balance->balance -= $model->amount;
                $balance->save();
            }

			return $model;
		}
		
        return false;
    }

    
    public function updateWebapp($request) {
        // $data = $request;
        // if($this->update($data)) {
		// 	return $this;
		// }

        // return false;
    }
}
