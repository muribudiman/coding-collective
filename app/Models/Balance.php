<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Balance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'balance'
    ];

    public $timestamps = true;

    public $incrementing = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'formattedBalanceAt',
        'formattedCreatedAt',
        'formattedUpdatedAt',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getFormattedCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M Y H:i:s');
    }
	
	public function getFormattedUpdatedAtAttribute($value)
    {
        return Carbon::parse($this->updated_at)->format('d M Y H:i:s');
    }
	
	public function getFormattedBalanceAtAttribute($value)
    {
        return generalMoney($this->balance, 2);
    }
}
