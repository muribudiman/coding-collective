<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\{
    OrderUser,
    Balance
};
use Inertia\Inertia;

class DashboardController extends Controller
{
    private $path_render;
	private $title_index;
	private $breadcrumb_index;

	public function __construct()
	{
		$this->path_render = "Dashboard/";
		$this->title_index = "Dashboard";

		$this->breadcrumb_index = [
			[
				'title' => $this->title_index,
				'url' => null,
				'active' => true
			]
		];
	}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $limit = abs((int) $request->query('per_page', 15));
        $page = abs((int) $request->query('page', 1));
        $queries = ['search', 'page'];

        $models = OrderUser::with(['user', 'order'])->applyFilters($request->only($queries))
                    ->where('user_id', Auth::user()->id)
                    ->orderBy('id', 'DESC')
                    ->paginateData($limit)
                    ->appends(request()->query());

        $balances = Balance::where('user_id', Auth::user()->id)->first();

        return Inertia::render($this->path_render . 'Index', [
            'models' => $models,
            'balances' => $balances,
            'title' => $this->title_index,
            'search' => $request->query('search', null),
            'breadcrumb' => $this->breadcrumb_index,
            'filters' => $request->all($queries),
            'start' => $limit * ($page - 1),
        ]);
    }
}
