<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ApiDocController extends Controller
{
    private $path_render;
	private $title_index;
	private $breadcrumb_index;

	public function __construct()
	{
		$this->path_render = "ApiDoc/";
		$this->title_index = "Api Doc";

		$this->breadcrumb_index = [
			[
				'title' => $this->title_index,
				'url' => null,
				'active' => true
			]
		];
	}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return Inertia::render($this->path_render . 'Index', [
            'title' => $this->title_index,
            'breadcrumb' => $this->breadcrumb_index,
            'api' => [
                'url' => route('api.v1.deposit'),
                'token' => Auth::user()->api_token,
                'body' => '{"order_id" : "INV-0000000001", "amount" : 50000000}',
            ]
        ]);
    }
}
