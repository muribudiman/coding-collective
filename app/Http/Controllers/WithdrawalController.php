<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\WithdrawalRequest;
use App\Models\{
    OrderUser,
    Balance,
    Order
};
use Inertia\Inertia;

class WithdrawalController extends Controller
{
    private $path_render;
	private $title_index;
	private $route_name;
	private $breadcrumb_index;

	public function __construct()
	{
		$this->path_render = "Withdrawal/";
		$this->title_index = "Withdrawal";
		$this->route_name = "withdrawals.";

		$this->breadcrumb_index = [
			[
				'title' => $this->title_index,
				'url' => null,
				'active' => true
			]
		];
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $balances = Balance::where('user_id', Auth::user()->id)->first();

        return Inertia::render($this->path_render . 'Create', [
            'form_type' => 'add',
            'balances' => $balances,
            'title' => $this->title_index,
            'route_url' => route($this->route_name . 'store'),
            'breadcrumb' => $this->breadcrumb_index,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WithdrawalRequest $request)
    {
        $time = time();
        $data = $request->validated();
        $data['user_id'] = Auth::user()->id;
        $data['order_id'] = "{$time}-{$data['account_number']}";
        $data['type'] = 'debit';

        Order::createWithdrawal($data);

        return redirect()->route('dashboard')->with('success', "Success");
    }
}
