<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Order;

class DepositController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
				'order_id' => [
					'required',
					Rule::unique('orders', 'order_id')
				],
				'amount' => [
					'required',
					'min:1',
					'numeric',
				],
            ]);
            
            if ($validator->fails()) {
                // $error = api_format(false, [$validator->errors()->toArray()], [], []);
                $error = $validator->errors()->toArray();
                $error['status'] = 2;
                return response()->json($error, 200);
            } else {
				$validated = $validator->validated();
                $validated['user_id'] = $request->user->id;
                $validated['type'] = 'kredit';
                $create = Order::createWebapp($validated);

                $response = [
                    'order_id' => $create->order_id,
                    'amount' => $create->amount,
                    'status' => 1,
                ];
				//$checkin = api_format(true, [], $response, []);
            	return response()->json($response, 200);	
            }
        } catch (\Exception $ex) {
            // $exception = api_format(false, ["message" => [$ex->getMessage()]], [], []);
            $exception = $ex->getMessage();
            return response()->json($exception, 200);
        }
    }
}
