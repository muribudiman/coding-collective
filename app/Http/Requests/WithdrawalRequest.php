<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class WithdrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $max = Auth::user()->balance->balance ?? 0;
        $rules = [
            'amount' => [
                'required',
                'min:10000',
                'max:' . $max,
                'numeric',
            ],
            'account_number' => [
                'required',
                'string',
                'min:7',
                'max:16',
            ],
        ];

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'amount' => 'Amount'
        ];

        return $attributes;
    }
}
